package com.selenium.atlassian.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.atlassian.baseclass.BaseClass;
import com.atlassian.editpage.pageobjects.ConfluenceActivityPage;
import com.atlassian.editpage.pageobjects.ConfluenceEditPage;
import com.atlassian.editpage.pageobjects.ConfluencePage;
import com.atlassian.editpage.pageobjects.HomePage;
import com.atlassian.editpage.pageobjects.LoginPage;

public class AtlassianEditRestrictionsTest{
	
	
		@Test
		public void ModifyRestrictions() 
		{
			
			System.setProperty("webdriver.chrome.driver", "C:\\Atlassian\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("https://id.atlassian.com/login");
			LoginPage loginPage = new LoginPage(driver);
			HomePage homePage = loginPage.LoginAs("shanika.maggamage@gmail.com", "1qaz2wsx@E");
			ConfluenceActivityPage confluenceActivityPage = homePage.ClickonConfluenceProductTile();
			ConfluencePage confluencePage = confluenceActivityPage.ClickonConfluence();
			ConfluenceEditPage confluenceEditPage = confluencePage.ClickonEditPage();
			confluenceEditPage.ClickonRestrictionsIcon();
			confluenceEditPage.SearchUser("shanika");
			confluenceEditPage.SelectUser();
			confluenceEditPage.EditPermission();
			confluenceEditPage.AddUser();
			confluenceEditPage.ApplyUser();
			
			//RemoveUser		
			confluenceEditPage.ClickonRestrictionsIcon();
			confluenceEditPage.RemoveUser();
			confluenceEditPage.ApplyUser();
			driver.quit();
			
			
		}
	
	
	

		
	

}
