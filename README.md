##Automation Technique

##Test Repository
	-A separate package has been created for this as well - This will be important to CI/CD 	integration in the future.
	-Page objects repo has been added as a dependency to the pom.xml
	-One Class has been implemented including main positive workflow scenarios.
	
	Naming convention and comments are included in order for the better readings and to follow the 	coding standards.
	TestNG annotations have been used 
#Test results 
	can be found in Testng reporting. 
	
##Prerequisites -
	1.Maven xml file should be in place
	2.Java should be run on the machine
	3.Eclipse should be setup in your machine
	4.Environment variable should be setup in machine [M2 and JAVA_HOME]
	5.Eclipse should be downloaded and paths should be setup in preferences in Eclipse
	6.Download the gecko driver for chrome as well. 
	7.Then click on ‘‘selenium-atlassian-test’ project and go to the AtlassianEditRestrictionsTest 	8.Java class.Change the location to your gecko driver download location and save.
	System.setProperty("webdriver.chrome.driver", "C:\\Atlassian\\chromedriver.exe");
	9.Then build the ‘selenium-atlassian-pageobjects’ repo
	10.For that, click on right on top of the project and select Run As->Maven Build . Then type 	‘clean install’ as the goal. Then Run the project
	11.Do the same for the ‘selenium-atlassian-test’ as well.
	12.Then Go to the ‘selenium-atlassian-test’ project . Launch the Class and right click on the 	class and run as TestNG
	
		
